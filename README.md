#[Bibliitk](https://bitbucket.org/atulramanathpai/bibliitk/overview)#
##Keep the library in your pocket !!!##
With Bibliitk, you will never forget to hand in a book. Immediately see what items you have in your possession and when to return it. If you still need the book, you can easily extend the loan. You do not have to ask if an item is available, because with this app you can easily search in the catalog of and make a reservation immediately. You get a mail when your reserved item is ready.
###makes it easy to###
* Find and discover titles
* Manage your account
* Get your library a/c information
###with this you can###
* Search the catalog of IIT-Kanpur's P.K.Kelkar library
* See what materials you have borrowed
* See when you have to return them the latest
* See how much fine you owe
* Renew your materials
* See which titles you have borrowed in the past
* Make reservations
* See what you have reserved
* Cancel your reservations
* And much more